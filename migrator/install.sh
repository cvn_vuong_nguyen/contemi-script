#!/usr/bin/env bash

CURRENT_PATH=$('pwd');

cp $CURRENT_PATH/migrator.sh /usr/local/bin/
cp $CURRENT_PATH/pegasus.phar /usr/local/bin/

echo 'Install successfully!';
