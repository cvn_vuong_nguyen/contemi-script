#!/usr/bin/env bash

MIGRATION_PATH=$('pwd')
ACTION=${1}
EXTRA=${2}

if [ "$1" == "create" ]
then
    if [ -z $2 ]
    then
        echo "Missing migration name";
        exit 1;
    fi

    /usr/local/bin/pegasus.phar phinx create -c ${MIGRATION_PATH}/phinx.php ${EXTRA}
    exit 0
fi

if [ "$1" == "up" ]
then
    /usr/local/bin/pegasus.phar generate-migration -f ${MIGRATION_PATH}/phinx.php -i=mariadb:10.4 -g
    exit 0
fi

if [ "$1" == "down" ]
then
    /usr/local/bin/pegasus.phar generate-migration -f ${MIGRATION_PATH}/phinx.php -i=mariadb:10.3 -gs
    exit 0
fi

if [ "$1" == "seed" ]
then

    if [ -z $2 ]
    then
        SEED=''
    else
        SEED="-s $2"
    fi
    /usr/local/bin/pegasus.phar phinx seed:run -c ${MIGRATION_PATH}/phinx.php ${SEED}
    exit 0
fi

echo 'Missing parameter';
