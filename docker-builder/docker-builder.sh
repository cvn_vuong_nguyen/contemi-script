#!/usr/bin/env bash

BUILD_PATH=$('pwd')
TAG=${1}
IMAGE=contemi/pegasus-application

if [ -z $1 ]
then
    echo "Missing tag";
    exit 1;
fi

which git;

if [[  $? -eq 0 ]] ; then
  echo "Pulling latest code from git...";
  git pull;
fi

if [ -f "$BUILD_PATH/composer.json" ]
then
  which composer;

  if [[  $? -eq 0 ]] ; then
    echo "composer dependencied...";
    composer update --ignore-platform-reqs;
  fi
fi

sudo rm $BUILD_PATH/storage/logs/*.log
sudo rm $BUILD_PATH/storage/logs/*.log.*

docker build --rm . -t  ${IMAGE}:${TAG}
docker push ${IMAGE}:${TAG}

echo 'Build and push successfully!';
