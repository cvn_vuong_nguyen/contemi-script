#!/usr/bin/env bash

URL=$1;
ROUTING=$2
ROUTING_STRING='';

if [ -z "$ROUTING" ]
then
    echo 'no routing';
else
    ROUTING_STRING='?routing='${ROUTING}
fi

if [ -z "$URL" ]
then
    echo "URL is empty"
else

FINAL_URL=${URL}'/_delete_by_query'${ROUTING_STRING};
echo ${FINAL_URL};
curl -XPOST ${FINAL_URL} -d '{
    "query" : {
        "match_all" : {}
    }
}'

fi
