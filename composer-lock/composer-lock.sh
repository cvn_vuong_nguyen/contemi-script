#!/usr/bin/env bash

PROJECT_PATH=$('pwd')

if [ -f "${PROJECT_PATH}/composer.json" ]
then
  which composer;

  if [[  $? -eq 0 ]] ; then
    echo "composer dependencied...";
    composer update --ignore-platform-reqs;
    echo "Wating for lock file...";
    sleep 2;
    rm -rf ${PROJECT_PATH}/vendor;
  else
    echo "composer is not installed";
    exit 1;
  fi
else
  echo "composer file not found";
  exit 1;
fi
